import subprocess

def read_status():
    """
    Returns the windscribe status
    """
    p = subprocess.Popen(["windscribe", "status"], stdout=subprocess.PIPE)
    out, err = p.communicate()
    
    # decode status bytes as utf-8 strings
    out = out.split(b"\n")
    # Remove trailing empty character
    if out[-1].decode("utf-8") == "":
        out = out[:-1]
    status = []
    for ligne in out:
        status.append(ligne.decode("utf-8"))
    return status

def windscribe_is_running(status=None):
    """
    Indicates whether the windscribe service is on.
    If status is None, the function manually checks the
    status.
    """
    if status is None:
        status = read_status()
    return status[0] != "Windscribe is not running"

def connected_to_a_server():
    """
    Indicates whether windscribe is connected to a server.
    """
    status = read_status()
    if (windscribe_is_running(status)):
        return status[-1].split(" ")[0] == "CONNECTED"
    else:
        return False
        

if __name__ == "__main__":
    print("Windscribe is running" if windscribe_is_running() else "Windscribe is not running")
    print("Windscribe connected" if connected_to_a_server() else "Windscribe not connected")